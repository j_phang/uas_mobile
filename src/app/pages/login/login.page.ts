import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { Alert } from 'selenium-webdriver';
import { AlertService } from 'src/app/services/alert.service';
import { snapshotToArray } from '../home/home.page';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  users = [];
  data = {
    username: '',
    password: ''
  };

  constructor(
    public router: Router,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public AlertService: AlertService,
    public modalController: ModalController
  ) {
    firebase.database().ref('users').on('value', resp => {
      this.users = [];
      this.users = snapshotToArray(resp);
    })
    console.log(this.users);
  }

  ngOnInit() {
  }

  login(){
    console.log(this.users);
    for(let data of this.users){
      if (this.data.username == data.username){
        if(this.data.password == data.password){
            this.router.navigate(['/home']);
            this.data = {
              username: '',
              password: ''
            };
        } else {
          this.AlertService.message('Username atau Password Salah !');
          this.data = {
            username: this.data.username,
            password: ''
          };
        }
      } 
    }
  }
}