import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-adds',
  templateUrl: './adds.page.html',
  styleUrls: ['./adds.page.scss'],
})

export class AddsPage {
  public createForm: FormGroup;
  ref = firebase.database().ref('laporan/');

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    public router: Router,
    public alertService: AlertService
  ) {
    this.createForm = this.formBuilder.group({
      nama: ['', Validators.required],
      perihal: ['', Validators.required],
      laporan: ['', Validators.required],
    });
  }

  createList(){
    let newInfo = firebase.database().ref('laporan/').push();
    newInfo.set(this.createForm.value);
    this.alertService.message('Berhasil Ditambahkan !');
    this.router.navigate(['/home']);
  }
}
