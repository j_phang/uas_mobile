import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsPage } from './adds.page';

describe('AddsPage', () => {
  let component: AddsPage;
  let fixture: ComponentFixture<AddsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
