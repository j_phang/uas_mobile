import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  laporan = [];
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public alertController: AlertController,
    public alertService: AlertService
  ){
    firebase.database().ref('laporan').on('value', resp => {
      this.laporan = [];
      this.laporan = snapshotToArray(resp);
    });
  }
  async delete(key){
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Apakah anda yakin ingin menghapus entri ini ?',
      buttons:[
        {
          text: 'Batalkan',
          role: 'batal',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Batal');
          }
        }, {
          text: 'Yakin',
          handler: () => {
            firebase.database().ref('laporan/'+ key).remove();
            this.alertService.message('Berhasil dihapuskan !');
          }
        }
      ]
    });
    await alert.present();
  }
  logout(){
    console.log("Anda Melakukan Logout");
    this.router.navigate(['/login']);
  }

}
export const snapshotToObject = snapshot => {
  let item = snapshot.val();
  item.key = snapshot.key;
  return item;
}

export const snapshotToArray = snapshot => {
  let returnArr = [];
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
}
