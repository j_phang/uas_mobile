import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDNF2TTHPz-PYN8wmzrqI6aJn7fX7nPnQk",
  authDomain: "uasmobilepoltek.firebaseapp.com",
  databaseURL: "https://uasmobilepoltek.firebaseio.com",
  projectId: "uasmobilepoltek",
  storageBucket: "uasmobilepoltek.appspot.com",
  messagingSenderId: "758133514796",
  appId: "1:758133514796:web:065cb45714d3237e"
}
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}
